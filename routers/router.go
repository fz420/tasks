package routers

import (
	"log"
	"net/http"
	"sktasks_be/controllers"
	"sktasks_be/models"

	jwt "github.com/appleboy/gin-jwt"

	"github.com/gin-gonic/gin"
)

var identityKey = "id"

func helloHandler(c *gin.Context) {
	// "id": "5d1f739ed007487b71927d12"
	// user, exists := c.Get(identityKey)
	// if !exists {
	// 	return make(jwt.MapClaims)
	// }
	// return claims.(jwt.MapClaims)
	claims := jwt.ExtractClaims(c)
	log.Println("helloHandler: ", claims)

	user, _ := c.Get(identityKey)
	log.Println("helloHandler user: ", user)
	// if !exists {
	// 	return make(jwt.MapClaims)
	// }

	c.JSON(200, gin.H{
		"userID":   claims["id"],
		"userName": user.(*models.User).Username,
		"phone":    user.(*models.User).Phone,
		"text":     "Hello World.",
	})
}

func InitRouter() *gin.Engine {
	// router := gin.Default()
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.LoadHTMLGlob("views/*")

	authMiddleware, err := jwt.New(&JWTMiddleware)
	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}
	router.POST("/login", authMiddleware.LoginHandler)
	router.POST("/register", controllers.UserRegister)
	router.NoRoute(authMiddleware.MiddlewareFunc(), func(c *gin.Context) {
		claims := jwt.ExtractClaims(c)
		log.Printf("NoRoute claims: %#v\n", claims)
		c.JSON(http.StatusNotFound, gin.H{
			"code": "PAGE_NOT_FOUND",
			"msg":  "Page not found",
		})
	})

	auth := router.Group("/auth")
	auth.GET("/refresh_token", authMiddleware.RefreshHandler)
	auth.Use(authMiddleware.MiddlewareFunc())
	{
		auth.GET("/hello", helloHandler)

		auth.GET("/user", controllers.GetUserInfo)
		auth.GET("/user/all", controllers.GetAllUsers)
		auth.POST("/user/update", controllers.UserUpdate)
		auth.POST("/user/logout", controllers.UserLogout)

		// 任务管理相关接口
		auth.GET("/task", controllers.GetTaskInfo)
		auth.GET("/task/all", controllers.GetAllTasks)
		auth.POST("/task/publish", controllers.TaskPublish)
		auth.POST("/task/sold", controllers.TaskSold)

	}

	return router
}
