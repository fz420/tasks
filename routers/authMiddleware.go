package routers

import (
	"log"
	"sktasks_be/dao"
	"sktasks_be/models"
	"time"

	"github.com/gin-gonic/gin"

	jwt "github.com/appleboy/gin-jwt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// func(c *gin.Context) (interface{}, error)
// loginHandler 登录接口验证方法
func loginHandler(c *gin.Context) (interface{}, error) {
	var user *models.User

	if err := c.ShouldBindJSON(&user); err != nil {
		return "", jwt.ErrMissingLoginValues
	}
	// originPwd := user.Password
	originPwd := user.P()

	user, err := dao.GetByPhone(user.Phone)
	if err != nil {
		log.Printf("用户查询失败：%v\n", err)
		return "", jwt.ErrFailedAuthentication
	}

	log.Println("loginHandler: ", user)
	if user.VerifyPwd(originPwd) {
		user.ClearPwd()
		return user, nil
	}

	return nil, jwt.ErrFailedAuthentication
}

// 未验证，自定义错误消息
func unauthorizedHandler(c *gin.Context, code int, msg string) {
	c.JSON(code, gin.H{
		"code": code,
		"msg":  msg,
		// "data": data,
	})
}

// 登录期间添加数据到 payload
func payloadHandler(data interface{}) jwt.MapClaims {
	if u, ok := data.(*models.User); ok {
		return jwt.MapClaims{
			identityKey: u.GetID(),
		}
	}
	return jwt.MapClaims{}

}

// identityHandler
func identityHandler(c *gin.Context) interface{} {
	claims := jwt.ExtractClaims(c)
	// "id": "5d1f739ed007487b71927d12",
	oid, err := primitive.ObjectIDFromHex(claims["id"].(string))
	if err != nil {
		log.Fatalf("identityHandler error: %v\n", err)
	}
	return &models.User{
		Id: oid,
	}
}

// fmt.Println("this is main init func")
var JWTMiddleware = jwt.GinJWTMiddleware{
	Realm:       "test zone",
	Key:         []byte("secret key"),
	Timeout:     time.Hour,
	IdentityKey: identityKey,
	MaxRefresh:  4 * time.Hour,

	// 登录期间添加数据到 payload
	PayloadFunc: payloadHandler,

	IdentityHandler: identityHandler,

	// 在登录接口中使用的验证方法，并返回成功后的用户对象
	Authenticator: loginHandler,

	// 登录后其他接口验证传入的 token 方法
	Authorizator: func(data interface{}, c *gin.Context) bool {
		if _, ok := data.(*models.User); ok {
			return true
		}
		return false
	},

	//  验证失败后设置错误信息
	Unauthorized: unauthorizedHandler,

	TokenLookup:   "header: Authorization, query: token, cookie: jwt",
	TokenHeadName: "Bearer",
	TimeFunc:      time.Now,
}
