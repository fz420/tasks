module sktasks_be

go 1.12

require (
	github.com/appleboy/gin-jwt v2.6.0+incompatible
	github.com/fatih/gomodifytags v0.0.0-20190716070638-2dd5565a4bd8 // indirect
	github.com/gin-gonic/gin v1.4.0
	github.com/go-ini/ini v1.42.0
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.3.0 // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a // indirect
	github.com/tidwall/pretty v1.0.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.0.2
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/sys v0.0.0-20190712062909-fae7ac547cb7 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190724185037-8aa4eac1a7c1 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.0
	gopkg.in/ini.v1 v1.42.0 // indirect
)
