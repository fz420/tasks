package controllers

import (
	"fmt"
	"log"
	"net/http"
	"sktasks_be/dao"
	"sktasks_be/models"
	"sktasks_be/pkg/e"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/gin-gonic/gin"
)

// TaskPublish  发布任务
func TaskPublish(c *gin.Context) {
	var task *models.Task

	// 1. 绑定与验证
	if err := c.ShouldBindJSON(&task); err != nil {
		log.Printf("任务绑定失败: %v\n", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": e.ERROR_VERIFY_FAIL,
			"msg":  e.MsgFlags[e.ERROR_VERIFY_FAIL],
			"data": err.Error(),
		})
		return
	}

	// 2. 验证成功
	// 检查是否合法用户
	user, err := getUser(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": e.ERROR_BAD_REQUEST,
			"msg":  e.MsgFlags[e.ERROR_BAD_REQUEST],
			"data": err.Error(),
		})
		return
	}

	log.Printf("用户信息: %+v\n", user)
	log.Printf("任务信息: %+v\n", task)

	// 检查用户是否有权发布任务
	res, isPub := user.IsPubTask()
	if !isPub {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": e.ERROR_FORBIDDEN_FAIL,
			"msg":  e.MsgFlags[e.ERROR_FORBIDDEN_FAIL],
			"data": fmt.Sprintf("发布任务限制: %v", res),
		})
		return
	}

	// 3. 发布任务
	task.SetPublisher(user.Id)         // 发布人
	task.SetStatus(models.Published)   // 设置任务默认状态
	task.SetPubAt(time.Now())          //任务发布时间
	task.SetTbStar(models.Star3)       // 设置淘宝星星3+
	task.SetTbDiamond(models.Diamond3) // 设置淘宝钻石3+
	task.SetTbAge(models.Year18)       // 设置淘宝在网时长 18年以前

	oid, err := dao.SaveOneTask(task)
	if err != nil {
		log.Printf("任务写入失败: %v\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": e.FAIL_DB_W,
			"msg":  e.MsgFlags[e.FAIL_DB_W],
			"data": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": e.SUCCESS,
		"msg":  e.MsgFlags[e.SUCCESS],
		"data": oid,
	})
}

func GetTaskInfo(c *gin.Context) {
	u, exist := c.Get(identityKey)
	if !exist {
		log.Printf("获取单人任务失败未找到用户")
		c.JSON(http.StatusBadRequest, gin.H{
			"code": e.ERROR_VERIFY_FAIL,
			"msg":  e.MsgFlags[e.ERROR_VERIFY_FAIL],
			"data": "获取单人任务失败，id解析失败",
		})
		return
	}

	oid, err := primitive.ObjectIDFromHex(u.(*models.User).GetID())
	if err != nil {
		log.Printf("用户id解析失败 %v\n", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": e.ERROR_VERIFY_FAIL,
			"msg":  e.MsgFlags[e.ERROR_VERIFY_FAIL],
			"data": "获取单人任务失败，id解析失败",
		})
		return
	}

	// log.Println("用户id为: ", oid)
	tasks, err := dao.GetTasksByUser(oid)
	if err != nil {
		log.Printf("任务查找失败 %v\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": e.FAIL_DB_R,
			"msg":  e.MsgFlags[e.FAIL_DB_R],
			"data": err.Error(),
		})
		return
	}

	// log.Printf("任务列表: %v\n", tasks)
	c.JSON(http.StatusOK, gin.H{
		"code": e.SUCCESS,
		"msg":  e.MsgFlags[e.SUCCESS],
		"data": tasks,
	})

}

func GetAllTasks(c *gin.Context) {
	tasks, err := dao.GetTasksAll()
	if err != nil {
		log.Printf("获取所有任务失败 %v\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": e.FAIL_DB_R,
			"msg":  e.MsgFlags[e.FAIL_DB_R],
			"data": err.Error(),
		})
		return
	}

	log.Println("任务列表为: ", tasks)
	c.JSON(http.StatusOK, gin.H{
		"code": e.SUCCESS,
		"msg":  e.MsgFlags[e.SUCCESS],
		"data": tasks,
	})

}

// TaskSold 任务下架
func TaskSold(c *gin.Context) {
	type Tasks struct {
		Ids []primitive.ObjectID `json:"ids"`
	}
	var tasks Tasks
	if err := c.ShouldBindJSON(&tasks); err != nil {
		log.Printf("任务绑定失败 %v\n", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": e.ERROR_BIND_FAIL,
			"msg":  e.MsgFlags[e.ERROR_BIND_FAIL],
			"data": err.Error(),
		})
		return
	}

	count, err := dao.ManySoldTask(tasks.Ids)
	if err != nil {
		log.Printf("任务下架失败: %v\n", err)
		c.JSON(http.StatusExpectationFailed, gin.H{
			"code": e.FAIL_DB_W,
			"msg":  e.MsgFlags[e.FAIL_DB_W],
			"data": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": e.SUCCESS,
		"msg":  e.MsgFlags[e.SUCCESS],
		"data": fmt.Sprintf("任务下架成功: %v条", count),
	})

}

// func getTasks(userId primitive.ObjectID) (tasks []models.Task) {
// 	filter := bson.M{"_id": userId}

// }
