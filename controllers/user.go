package controllers

import (
	"errors"
	"log"
	"net/http"
	"sktasks_be/dao"
	"sktasks_be/models"
	"sktasks_be/pkg/e"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/gin-gonic/gin"
)

// GetUserInfo 获取单个用户信息
func GetUserInfo(c *gin.Context) {
	user, err := getUser(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": e.ERROR_BAD_REQUEST,
			"msg":  e.MsgFlags[e.ERROR_BAD_REQUEST],
			"data": err.Error(),
		})
		return
	}

	user.ClearPwd()

	c.JSON(http.StatusOK, gin.H{
		"code": e.SUCCESS,
		"msg":  e.MsgFlags[e.SUCCESS],
		"data": user,
	})

}

// GetAllUsers 获取所有用户信息
func GetAllUsers(c *gin.Context) {
}

// UserUpdate 更新用户用户
func UserUpdate(c *gin.Context) {}

// UserRegister 用户注册接口
func UserRegister(c *gin.Context) {
	var user *models.User

	// 1. 绑定与验证
	if err := c.ShouldBindJSON(&user); err != nil {
		log.Printf("user verify error: %v\n", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": e.ERROR_VERIFY_FAIL,
			"msg":  e.MsgFlags[e.ERROR_VERIFY_FAIL],
			"data": err.Error(),
		})
		return
	}
	log.Println("用户信息：", user)

	// 2. 验证成功，初始化默认字段
	user.SetRegTime(time.Now())
	user.SetExpTime(user.RegTime().Add(userTryTimeout)) // 新用户试用，默认 now+5天
	user.SetStatus(models.Activated)                    // 设置用户状态
	user.SetPwd(user.P())                               // 加密用户密码

	// 3. 写入数据库
	oid, err := dao.SaveOne(user)
	if err != nil {
		log.Printf("用户写入数据库失败: %v\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": e.FAIL_DB_W,
			"msg":  e.MsgFlags[e.FAIL_DB_W],
			"data": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": e.SUCCESS_REG,
		"msg":  e.MsgFlags[e.SUCCESS_REG],
		"data": oid,
	})
	return
}

// UserLogin 用户登录
func UserLogin(c *gin.Context) {
	// user := models.User{}
	var user *models.User

	// 1. 绑定与验证
	if err := c.ShouldBindJSON(&user); err != nil {
		log.Printf("用户验证失败: %v\n", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": e.ERROR_VERIFY_FAIL,
			"msg":  e.MsgFlags[e.ERROR_VERIFY_FAIL],
			"data": err.Error(),
		})
		return
	}
	originPwd := user.P()
	// 2. 按手机号查找用户
	user, err := dao.GetByPhone(user.Phone)
	if err != nil {
		log.Printf("用户查找失败: %v\n", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": e.ERROR_NOT_EXIST_USER,
			"msg":  e.MsgFlags[e.ERROR_NOT_EXIST_USER],
			"data": err.Error(),
		})
		return
	}

	if user.VerifyPwd(originPwd) {
		user.ClearPwd()
		c.JSON(http.StatusOK, gin.H{
			"code": e.SUCCESS,
			"msg":  e.MsgFlags[e.SUCCESS],
			"data": user,
		})
		return
	}

	c.JSON(http.StatusBadRequest, gin.H{
		"code": e.ERROR_VERIFY_FAIL,
		"msg":  e.MsgFlags[e.ERROR_VERIFY_FAIL],
		"data": "密码验证失败",
	})
	return

}
func UserLogout(c *gin.Context) {}

// getUser 获取用户模型
func getUser(c *gin.Context) (user *models.User, err error) {

	u, exist := c.Get(identityKey)
	if !exist {
		return nil, errors.New("GetUser: 请求id获取失败")
	}

	oid, err := primitive.ObjectIDFromHex(u.(*models.User).GetID())
	if err != nil {
		// log.Println("GetByID: 查找用户失败")
		return nil, errors.New("GetUser: 用户id解析失败")
	}

	user, err = dao.GetByID(oid)
	if err != nil {
		return nil, errors.New("GetByID: 查找用户失败")
	}

	return user, nil

}
