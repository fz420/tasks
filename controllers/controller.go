package controllers

import (
	"log"
	"sktasks_be/pkg/setting"
	"time"
)

// 新用户试用时长，默认至少 5天
var userTryTimeout time.Duration

var identityKey = "id"

func init() {
	// 获取 db 配置
	sec, err := setting.Cfg.GetSection("app")
	if err != nil {
		log.Fatalf("Fail to get section 'database': %v", err)
	}
	// 新用户默认试用期至少 5天
	dur, _ := time.ParseDuration("120h")
	userTryTimeout = sec.Key("USER_TRY_TIMEOUT").MustDuration(dur)
	// log.Fatalf("试用时间为: %v\n", tryTimeout)

}
