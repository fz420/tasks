package utils

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

//GeneratePassword  生成加密密码
func GeneratePassword(pwd string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.DefaultCost)

	if err != nil {
		log.Printf("generate password error: %v\n", err)
		return "", err
	}

	return string(hash), nil
}

// userPwd 用户的原始密码
// hashPwd 数据库hashed的密码
// VerifyPassword 验证用户密码O
func VerifyPassword(hashPwd string, userPwd string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashPwd), []byte(userPwd))

	if err != nil {
		log.Printf("verify password error: %v\n", err)
		return false
	}

	return true
}
