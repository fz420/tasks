package dao

import (
	"context"
	"log"
	"sktasks_be/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// SaveOne save one user from User model
func SaveOne(user *models.User) (oid string, err error) {
	res, err := userCollection.InsertOne(context.Background(), user)
	if err != nil {
		log.Printf("保存单个用户失败: %v\n", err)
		return "", err
	}
	return res.InsertedID.(primitive.ObjectID).Hex(), nil
}

// SaveMany save many user from byte slice
// func SaveMany(user []models.User) error {
// 	var us []interface{}

// 	for _, u := range  user {
// 		us = append(us, u)
// 	}

// 	_, err := userCollection.InsertMany(context.Background(), us)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// }

// GetByID get user by id
func GetByID(userID primitive.ObjectID) (user *models.User, err error) {
	result := userCollection.FindOne(context.Background(), bson.M{"_id": userID})

	err = result.Decode(&user)
	if err != nil {
		return &models.User{}, err
	}
	// log.Printf("find one user: %v\n", user)
	return user, nil
}

func GetByPhone(phone string) (user *models.User, err error) {
	result := userCollection.FindOne(context.Background(), bson.M{"phone": phone})

	err = result.Decode(&user)
	if err != nil {
		return &models.User{}, err
	}

	// log.Printf("find one user: %v\n", user)
	return user, nil
}

func GetAll() (users []*models.User, err error) {
	cur, err := userCollection.Find(context.Background(), nil, nil)
	if err != nil {
		log.Fatal(err)
	}

	var user *models.User
	for cur.Next(context.Background()) {
		err := cur.Decode(&user)
		if err != nil {
			log.Fatal(err)
		}
		users = append(users, user)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
	cur.Close(context.Background())
	return users, nil
}

// func DeleteByID(models.User.ID) error {}
// func DeleteMany([]models.User) error {}
// func UpdateByID(models.User, models.User.ID) error {}
// func LockByID(models.User.ID) error {}
