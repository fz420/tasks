package dao

import (
	"context"
	"log"
	"sktasks_be/models"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// SaveOneTask 保存单个任务
func SaveOneTask(t *models.Task) (oid string, err error) {
	res, err := taskCollection.InsertOne(context.Background(), t)
	if err != nil {
		log.Printf("保存单个任务失败: %v\n", err)
		return "", err
	}

	return res.InsertedID.(primitive.ObjectID).Hex(), nil
}

func ManySoldTask(ids []primitive.ObjectID) (count int, err error) {
	var writes []mongo.WriteModel

	var updatesFilter []bson.M
	var uv bson.M
	uv = bson.M{"$set": bson.M{"update_at": time.Now(), "status": models.Takeoffline}}

	for _, id := range ids {
		updatesFilter = append(updatesFilter, bson.M{"_id": id})
	}

	for _, f := range updatesFilter {
		model := mongo.NewUpdateManyModel().SetFilter(f).SetUpdate(uv)
		writes = append(writes, model)
	}

	res, err := taskCollection.BulkWrite(context.Background(), writes)
	if err != nil {
		return 0, err
	}
	return int(res.ModifiedCount), nil

}

func GetTasksByUser(userId primitive.ObjectID) (tasks []models.Task, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	filter := bson.M{"publisher": userId}

	cur, err := taskCollection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	for cur.Next(ctx) {
		var t models.Task
		if err := cur.Decode(&t); err != nil {
			log.Printf("任务解码失败 %v\n", err)
			// return nil, err
			continue
		}
		tasks = append(tasks, t)
	}

	if err := cur.Err(); err != nil {
		// log.Printf("数据库游标操作失败 %v\n", err)
		return nil, err
	}

	return tasks, nil

}

func GetTasksAll() (tasks []models.Task, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"count": bson.M{"$gt": 0}}

	cur, err := taskCollection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	for cur.Next(ctx) {
		var t models.Task
		if err := cur.Decode(&t); err != nil {
			log.Printf("任务解码失败 %v\n", err)
			continue
		}
		tasks = append(tasks, t)
	}

	if err := cur.Err(); err != nil {
		return nil, err
	}
	return tasks, nil

}
