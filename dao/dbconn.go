package dao

import (
	"context"
	"fmt"
	"log"
	"sktasks_be/pkg/setting"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	err         error
	dbType      string
	dbName      string
	user        string
	password    string
	host        string
	connTimeout time.Duration
	dbURI       string

	db             *mongo.Database
	userCollection *mongo.Collection
	taskCollection *mongo.Collection
)

func init() {

	// 获取 db 配置
	sec, err := setting.Cfg.GetSection("database")
	if err != nil {
		log.Fatalf("Fail to get section 'database': %v", err)
	}

	dbType = sec.Key("TYPE").String()
	dbName = sec.Key("NAME").String()
	user = sec.Key("USER").String()
	password = sec.Key("PASSWORD").String()
	host = sec.Key("HOST").String()
	connTimeout = sec.Key("CONN_TIMEOUT").MustDuration()

	dbURI = fmt.Sprintf("%s://%s", dbType, host)

	// 连接 db server
	ctx, cancel := context.WithTimeout(context.Background(), connTimeout)
	defer cancel()

	// "mongodb+srv://<username>:<password>@<cluster-address>/test"
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(dbURI))
	if err != nil {
		log.Fatalf("Fail to conn db server: %v", err)
	}

	// check the connection
	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatalf("Fail to ping db server: %v", err)
	}
	// 连接到 db
	db := client.Database(dbName)

	// 连接文档
	userCollection = db.Collection("user")
	taskCollection = db.Collection("task")

}
