package e

var MsgFlags = map[int]string{
	SUCCESS: "ok",
	ERROR:   "fail",

	SUCCESS_REG: "用户注册成功",

	ERROR_EXIST_USER:     "用户已存在",
	ERROR_NOT_EXIST_USER: "用户不存在",
	ERROR_REG_FAIL:       "用户注册失败",
	ERROR_VERIFY_FAIL:    "用户验证失败",
	ERROR_BAD_REQUEST:    "请求异常",

	ERROR_FORBIDDEN_FAIL: "权限拒绝",
	ERROR_INVALID_PARAMS: "请求参数错误",
	ERROR_BIND_FAIL:      "请求绑定失败",

	FAIL_CONN_DB: "数据库连接异常",
	FAIL_DB_W:    "数据库写失败",
	FAIL_DB_R:    "数据库读失败",
}

func GetMsg(code int) string {
	msg, ok := MsgFlags[code]
	if ok {
		return msg
	}

	return MsgFlags[ERROR]
}
