package setting

import (
	"log"
	"time"

	"github.com/go-ini/ini"
)

var (
	Cfg *ini.File

	RunMode      string
	HTTPPort     int
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
	LogPath      string

	PageSize  int
	JwtSecret string
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var err error
	Cfg, err = ini.Load("conf/app.ini")
	if err != nil {
		log.Fatalf("Fail to parse 'conf/app.ini': %v", err)
	}

	LoadBase()
	LoadServer()
	LoadApp()
}

func LoadBase() {
	RunMode = Cfg.Section("").Key("RUN_MODE").MustString("debug")
}

func LoadServer() {
	sec, err := Cfg.GetSection("server")
	if err != nil {
		log.Fatalf("Fail to get section 'server': %v", err)
	}

	RunMode = Cfg.Section("").Key("RUN_MODE").MustString("debug")
	HTTPPort = sec.Key("HTTP_PORT").MustInt(8000)

	dur, err := time.ParseDuration("60s")
	ReadTimeout = sec.Key("READ_TIMEOUT").MustDuration(dur)
	WriteTimeout = sec.Key("WRITE_TIMEOUT").MustDuration(dur)
	LogPath = sec.Key("LOG_PATH").MustString(".")

}

// loadApp 获取 app 配置
func LoadApp() {
	sec, err := Cfg.GetSection("app")
	if err != nil {
		log.Fatalf("Fail to get section 'app': %v", err)
	}

	JwtSecret = sec.Key("JWT_SECRET").MustString("!@)*#)@U#@*!@!)")
	PageSize = sec.Key("PAGE_SIZE").MustInt(10)

}
