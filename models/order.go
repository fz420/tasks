package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Order struct {
	Id             primitive.ObjectID `json:"id,omitempty" bson:"id,omitempty"`
	Payer          primitive.ObjectID `json:"payer" bson:"payer"`                 //付款人
	Amount         int                `json:"amount,omitempty" bson:"amount"`     // 支付金额
	Content        string             `json:"content,omitempty" bson:"content"`   // 订单内容
	Category       int                `json:"category,omitempty" bson:"category"` // 订单类别，单次，包月
	PayAt          time.Time          `json:"pay_at,omitempty" bson:"pay_at"`     // 支付时间
	CreateAt       time.Time          `json:"create_at" bson:"create_at"`
	SentAt         time.Time          `json:"sent_at" bson:"sent_at"`
	Status         int                `json:"status,omitempty" bson:"status"`                     // 支付状态
	ThirdPartyNo   string             `json:"third_party_no,omitempty" bson:"third_party_no"`     // 第三方支付号码
	ThirdPartyName string             `json:"third_party_name,omitempty" bson:"third_party_name"` // 第三方支付名称
}

