package models

import (
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Task struct {
	Id        primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Publisher primitive.ObjectID `json:"publisher,omitempty" bson:"publisher"`            // 发布人
	Count     int                `json:"count" bson:"count" binding:"required"`           // 任务数
	Cost      int                `json:"cost" bson:"cost" binding:"gte=10,required"`      // 任务金额
	Profit    int                `json:"profit" bson:"profit" binding:"required"`         // 任务佣金
	PlatForm  string             `json:"plat_form" bson:"plat_form"  binding:"required"`  // 发布平台
	FanLiWay  int                `json:"fan_li_way" bson:"fan_li_way" binding:"required"` // 返利方式
	WuLiuWay  int                `json:"wu_liu_way" bson:"wu_liu_way" binding:"required"` // 物流方式
	GiftWay   int                `json:"gift_way" bson:"gift_way" binding:"required"`     // 礼物方式
	PubAt     time.Time          `json:"pub_at,omitempty" bson:"pub_at"`                  // 发布时间
	UpdateAt  time.Time          `json:"update_at" bson:"update_at"`
	Status    int                `json:"status,omitempty" bson:"status"` // 发布状态

	TbSex     int      `json:"tb_sex,omitempty" bson:"tb_sex"`         // 淘宝帐户性别
	TbStar    int      `json:"tb_star,omitempty" bson:"tb_star"`       // 淘宝星星
	TbDiamond int      `json:"tb_diamond,omitempty" bson:"tb_diamond"` //淘宝钻石
	TbAge     int      `json:"tb_age,omitempty" bson:"tb_age"`         // 淘宝在网时长
	Area      []int    `json:"area,omitempty" bson:"area"`             // 限制地区
	Deny      []string `json:"deny,omitempty" bson:"deny"`             // 限制花信
}

// GetID 获取任务id  hex string
func (t *Task) GetID() string {
	return t.Id.Hex()
}

func (t *Task) SetPublisher(publisherID primitive.ObjectID) {
	t.Publisher = publisherID
}

// S 获取任务状态
func (t *Task) S() string {
	/*
		1 发布中 publishing
		2 竞拍中 bidding
		3 完成  completed
		4 下架  takeoffline
	*/
	switch t.Status {
	case Published:
		return "Published"
	case Bidding:
		return "Bidding"
	case Completed:
		return "Completed"
	case Takeoffline:
		return "Takeoffline"
	default:
		return ""
	}
}

// SetStatus 设置任务状态
func (t *Task) SetStatus(s int) {
	switch s {
	case Published:
		t.Status = Published
	case Bidding:
		t.Status = Bidding
	case Completed:
		t.Status = Completed
	case Takeoffline:
		t.Status = Takeoffline
	default:
		log.Fatalf("任务状态设置异常: %v\n", s)
	}
}

func (task *Task) SetPubAt(t time.Time) {
	task.PubAt = t
}

func (task *Task) SetTbStar(s int) {
	task.TbStar = s
}

func (task *Task) SetTbDiamond(d int) {
	task.TbDiamond = d
}

func (task *Task) SetTbAge(a int) {
	task.TbAge = a
}

// C 任务数
func (task *Task) C() int {
	return task.Count
}

func (task *Task) SetCount(a int) {
	task.Count = a

}

func (task *Task) SubCount() {
	task.Count -= 1
}
