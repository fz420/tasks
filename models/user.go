package models

import (
	"log"
	"sktasks_be/utils"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	Id             primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Username       string             `json:"username,omitempty" bson:"username,omitempty"`                      //用户名
	Password       string             `json:"password" bson:"password" binding:"gte=8,required"`                 //用户密码
	Phone          string             `json:"phone" bson:"phone" binding:"len=11,required"`                      //用户手机
	QQ             string             `json:"qq" bson:"qq" binding:"omitempty,gte=5"`                            //用户QQ
	Wangwang       string             `json:"wangwang" bson:"wangwang" binding:"omitempty,gte=2"`                //用户旺旺
	InvitationCode string             `json:"invitation_code" bson:"invitation_code" binding:"omitempty,len=11"` //邀请人手机
	InvitationBit  int                `json:"invitation_bit" bson:"invitation_bit"`                              // 邀请标识
	CreateAt       time.Time          `json:"create_at" bson:"create_at"`                                        // 注册时间
	ExpirationAt   time.Time          `json:"expiration_at" bson:"expiration_at"`                                // 过期时间
	Status         int                `json:"status" bson:"status"`                                              // 用户状态：0 locked 1 activied
	TaskCounts     int                `json:"task_counts" bson:"task_counts"`                                    // 已发布的任务数
	TaskLevel      int                `json:"task_level" bson:"task_level"`                                      // 任务级别

}

// GetID 返回用户 objectId， hex string
func (user *User) GetID() string {
	return user.Id.Hex()
}

// RegTime 用户注册时间
func (user *User) RegTime() time.Time {
	return user.CreateAt
}

// SetRegTime
// 设置用户注册时间
func (user *User) SetRegTime(t time.Time) {
	user.CreateAt = t
}

// ExpTime 用户帐户过期时间
func (user *User) ExpTime() time.Time {
	return user.ExpirationAt

}

// SetExpTime 设置用户帐户过期时间
func (user *User) SetExpTime(t time.Time) {
	user.ExpirationAt = t
}

// P 获取用户密码
func (user *User) P() string {
	return user.Password
}

// SetPwd 设置用户密码
func (user *User) SetPwd(originPwd string) {
	var err error
	user.Password, err = utils.GeneratePassword(originPwd)
	if err != nil {
		log.Fatalf("用户生成密码错误: %v\n", err)
	}
}

// ClearPwd 清空用户密码
func (user *User) ClearPwd() {
	user.Password = ""
}

// VerifyPwd 验证用户密码
func (user *User) VerifyPwd(originPwd string) bool {
	return utils.VerifyPassword(user.Password, originPwd)
}

// S 获取用户状态
func (user *User) S() int {
	// return user.Status
	switch user.Status {
	case Locked:
		return Locked
	case Deleted:
		return Deleted
	default:
		return Activated
	}
}

// SetStatus 设置用户状态: 0 locked 1 activated 2 deleted
func (user *User) SetStatus(s int) {
	// user.Status = s
	switch s {
	case Locked:
		user.Status = Locked
	case Activated:
		user.Status = Activated
	case Deleted:
		user.Status = Deleted
	default:
		log.Fatalf("用户状态设置异常: %v\n", s)
	}
}

// IsPubTask 检查是否能发布任务
// 新用户试用期检查，限制发布（检查 user.Expiration）
// 用户信息完整性检查，限制发布 (检查 user.qq， user.wangwang)
// 用户任务级别超额检查，限制发布（检查 user.tasklevel）
// 用户状态检查，限制发布（检查 user.status）
func (user *User) IsPubTask() (s string, b bool) {
	switch {
	case time.Now().After(user.ExpTime()):
		return "试用时间到期", false
	case user.QQ == "" || user.Wangwang == "":
		return "用户信息不完整", false
	case user.TaskCounts > user.TaskLevel:
		return "任务数超额", false
	case user.S() != Activated:
		return "帐户锁定", false
	default:
		return "", true
	}
}

// AddTaskCounts 任务数增加 1
func (user *User) AddTaskCounts() {
	user.TaskCounts += 1
}

// ResetTaskCounts
func (user *User) ResetTaskCounts() {
	user.TaskCounts = 0
}

// SetTaskLevel 设置任务级别
func (user *User) SetTaskLevel(level int) {
	/*
		月费	条数			备注
		58		<=100平均约 	3条/天
		88		<=300平均约 	300/30 =  10条/天
		138		<=800平均约 	800/30 = 26条/天
	*/
	switch level {
	case Vip1:
		user.TaskLevel = Vip1
	case Vip2:
		user.TaskLevel = Vip2
	case Vip3:
		user.TaskLevel = Vip3
	default:
		user.TaskLevel = Vip1
	}
}

// UpdateTaskLevel
// 更新用户任务级别，当前剩余任务级别数 + 新增加的任务级别数
func (user *User) UpdateTaskLevel(level int) {
	// curTaskLevel = user.TaskLevel
	user.TaskLevel += level
}

// IsNewUser 新手期间
func (user *User) IsNewUser() bool {
	return time.Now().Before(user.ExpTime())
}

// LastTasks  返回用户最新的任务
// func (user *User) LastTasks() ([]Task, error) {}

// IOI 返回关注的用户
// func (user *User) IOI() ([]User, error) {}
