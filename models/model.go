package models

const (
	Vip1 = 100
	Vip2 = 300
	Vip3 = 800

	_ = iota
	Locked
	Activated
	Deleted
)

const (
	_ = iota
	Published
	Bidding
	Completed
	Takeoffline
)

const (
	_ = iota
	Star1
	Star2
	Star3
	Star4
	Star5
)

const (
	_ = iota
	Diamond1
	Diamond2
	Diamond3
	Diamond4
	Diamond5
)

const (
	_ = 14 + iota
	Year15
	Year16
	Year17
	Year18
)
